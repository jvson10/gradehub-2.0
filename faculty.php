<?php
    session_start();
    if(isset($_SESSION['userID'])){
        //echo "Welcome! " .$_SESSION['userID'];
    }
    include ("conn.php");
    $result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
    $row_uid = mysqli_fetch_array($result_uid);
?>
<html>
<head>
    <title>Faculty</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <h1>Welcome! Teacher <?php echo $row_uid['user_code']?></h1>
    <h3>Classes</h3>
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col col-lg-5">
                <table>
                    <td><form action="years_list.php"><button class="btn btn-warning">Years</button></form></td>
                    <td><form action="sems_list.php"><button class="btn btn-warning">Semesters</button></form></td>
                    <td><form action="class_search.php" method="GET"><input type="search" name="class_code" placeholder="Class Code" style="margin-left:5px;"><button style="margin-top:5px;margin-left:5px;" class="btn btn-primary" type="submit">Search</button></form></td>
                </table>
            </div>
            <div class="col col-lg-auto">
                <table class="table table-hover">
                    <thead>
                        <th>Class #</th>
                        <th>Class Code</th>
                        <th>Class Name</th>
                    </thead>
                    <tbody>
                        <?php
                        $result = mysqli_query($conn,"SELECT * FROM class_table WHERE teacher_userid = ".$_SESSION['userID']);
                        
                        //start loop
                        while($row = mysqli_fetch_array($result)){
                        ?>
                            <tr>
                                <form action="student_list.php" method="get">
                                    <td><input type="number" name="class_id" value="<?php echo $row['class_id']?>" readonly></td>
                                    <td><?php echo $row['class_code']?></td>
                                    <td><?php echo $row['class_name']?></td>
                                    <td><button class="btn btn-warning" type="submit">Update</button></td>
                                </form>
                            </tr>
                        
                        <?php
                        //end loop
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col col-lg-2"></div>
        </div>
    </div>
</body>
</html>