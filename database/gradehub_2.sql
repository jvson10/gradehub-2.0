-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2019 at 03:15 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gradehub_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_table`
--

CREATE TABLE `activity_table` (
  `activity_id` int(11) NOT NULL,
  `activity_name` varchar(30) NOT NULL,
  `activity_grade` float NOT NULL,
  `activity_class_id` int(11) NOT NULL,
  `student_uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_table`
--

INSERT INTO `activity_table` (`activity_id`, `activity_name`, `activity_grade`, `activity_class_id`, `student_uid`) VALUES
(1, 'Quiz 1', 1, 1, 2),
(2, 'Quiz 2', 1, 1, 2),
(3, 'Quiz 3', 3, 1, 2),
(4, 'preMidterm', 1, 1, 2),
(5, 'Quiz 4', 1, 1, 2),
(6, 'Quiz 5', 1.2, 1, 2),
(7, 'Midterm', 2.3, 1, 2),
(8, 'preFinal', 1, 1, 2),
(9, 'Final', 2.1, 1, 2),
(10, 'bonus', 1.5, 1, 2),
(11, 'Quiz 1', 1.4, 1, 3),
(12, 'Quiz 2', 1.6, 1, 3),
(13, 'preMidterm', 2.1, 1, 3),
(14, 'Midterm', 2, 1, 3),
(15, 'Quiz 3', 2.1, 1, 3),
(16, 'preFinal', 2.3, 1, 3),
(17, 'Quiz 4', 2.1, 1, 3),
(18, 'Final', 3, 1, 3),
(20, 'bonus', 1.4, 1, 3),
(21, 'Quiz 1', 3, 1, 4),
(22, 'Quiz 2', 3, 1, 4),
(23, 'preMidterm', 3, 1, 4),
(24, 'Midterm', 3, 1, 4),
(25, 'preFinal', 2, 1, 4),
(26, 'Final', 1, 1, 4),
(27, 'Quiz 1', 3, 1, 6),
(28, 'Quiz 2', 3, 1, 6),
(29, 'preMidterm', 2, 1, 6),
(30, 'Midterm', 1, 1, 6),
(31, 'preFinal', 2, 1, 6),
(32, 'Final', 2, 1, 6),
(33, 'Quiz 1', 3, 1, 7),
(34, 'preMidterm', 3, 1, 7),
(35, 'Midterm', 3, 1, 7),
(36, 'preFinal', 3, 1, 7),
(37, 'Final', 3, 1, 7),
(38, 'bonus', 1, 1, 7),
(39, 'Quiz 1', 3, 1, 8),
(40, 'preMidterm', 3, 1, 8),
(41, 'Midterm', 1, 1, 8),
(42, 'preFinal', 3, 1, 8),
(43, 'Final', 1, 1, 8),
(44, 'Quiz 1', 2.5, 1, 2),
(45, 'Quiz 1', 2, 1, 2),
(46, 'Quiz 1', 3, 1, 2),
(47, 'bonus', 2, 1, 4),
(48, 'bonus', 2, 1, 4),
(49, 'bonus', 2, 1, 4),
(50, 'Quiz 1', 2, 1, 8),
(51, 'Quiz 1', 2, 2, 8),
(52, 'Quiz 1', 2, 2, 8),
(53, 'Quiz 1', 2, 2, 8),
(54, 'Quiz 1', 2, 2, 8),
(55, 'Midterm', 1, 2, 8),
(56, 'preFinal', 3, 2, 8),
(57, 'preFinal', 3, 2, 8),
(58, 'preFinal', 3, 2, 8),
(59, 'Final', 1, 2, 8),
(60, 'bonus', 3, 2, 8),
(61, 'bonus', 3, 2, 8),
(62, 'bonus', 3, 2, 8),
(63, 'bonus', 3, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `class_table`
--

CREATE TABLE `class_table` (
  `class_id` int(11) NOT NULL,
  `class_name` varchar(30) NOT NULL,
  `class_code` varchar(10) NOT NULL,
  `teacher_userid` tinyint(4) NOT NULL,
  `sem` enum('1','2') NOT NULL,
  `year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_table`
--

INSERT INTO `class_table` (`class_id`, `class_name`, `class_code`, `teacher_userid`, `sem`, `year`) VALUES
(1, 'Computer Stuff', 'CS1111', 1, '1', 2019),
(2, 'Computer Stuff II', 'CS1112', 1, '2', 2019);

-- --------------------------------------------------------

--
-- Table structure for table `student_classes_table`
--

CREATE TABLE `student_classes_table` (
  `student_userid` int(11) NOT NULL,
  `student_class` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_classes_table`
--

INSERT INTO `student_classes_table` (`student_userid`, `student_class`) VALUES
(2, 1),
(3, 1),
(4, 1),
(6, 2),
(7, 2),
(8, 2),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_table`
--

CREATE TABLE `users_table` (
  `user_id` tinyint(4) NOT NULL,
  `user_code` int(10) NOT NULL,
  `user_pass` int(10) NOT NULL,
  `user_Fname` varchar(30) NOT NULL,
  `user_Lname` varchar(30) NOT NULL,
  `user_role` enum('faculty','student') NOT NULL,
  `classes_id` int(11) NOT NULL,
  `user_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_table`
--

INSERT INTO `users_table` (`user_id`, `user_code`, `user_pass`, `user_Fname`, `user_Lname`, `user_role`, `classes_id`, `user_year`) VALUES
(1, 15102571, 1234, 'Good', 'Teacher', 'faculty', 1, 20),
(2, 15102572, 1234, 'John', 'Doe', 'student', 1, 3),
(3, 15102573, 1234, 'Doe', 'Knott', 'student', 1, 3),
(4, 15102574, 1234, 'Kevin', 'Malone', 'student', 1, 3),
(5, 15102575, 1234, 'Andy', 'Bernard', 'faculty', 2, 21),
(6, 15102576, 1234, 'Michael', 'Scott', 'student', 2, 3),
(7, 15102577, 1234, 'Jim', 'Halpert', 'student', 2, 3),
(8, 15102578, 1234, 'Dwight', 'Schrute', 'student', 2, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_table`
--
ALTER TABLE `activity_table`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `class_table`
--
ALTER TABLE `class_table`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `users_table`
--
ALTER TABLE `users_table`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_code` (`user_code`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_table`
--
ALTER TABLE `activity_table`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `class_table`
--
ALTER TABLE `class_table`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_table`
--
ALTER TABLE `users_table`
  MODIFY `user_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
