<?php
    session_start();
    if(isset($_SESSION['userID'])){
        //echo "Welcome! " .$_SESSION['userID'];
    }
    include ("conn.php");
    $result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
    $row_uid = mysqli_fetch_array($result_uid);
?>
<html>
<head>
    <title><?php echo $row_uid['user_Fname']?> <?php echo $row_uid['user_Lname']?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <h1>Welcome! Student <?php echo $row_uid['user_code']?></h1>
    <h3>Classes</h3>
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col col-lg-2">
            </div>
            <div class="col col-lg-auto">
                <table class="table table-hover">
                    <thead>
                        <th>Class #</th>
                        <th>Class code</th>
                        <th>Class name</th>
                        <th></th>
                    </thead>
                    <tbody>
                    <?php
                        $result_classes = mysqli_query($conn,"SELECT student_class FROM student_classes_table WHERE student_userid = ".$_SESSION['userID']);
                        
                        while($row_classes = mysqli_fetch_array($result_classes)){

                            $res_cl = mysqli_query($conn,"SELECT * FROM class_table WHERE class_id = ".$row_classes['student_class']);
                            $row_cl = mysqli_fetch_array($res_cl);
                    ?>
                        <tr>
                            <form action="student_activities_view.php" method="GET">
                            <td><input type="number" name="class_id" value="<?php echo $row_classes['student_class']?>"></td>
                            <td><?php echo $row_cl['class_code']?></td>
                            <td><?php echo $row_cl['class_name']?></td>
                            <td><button class="btn btn-warning" type="submit">Activities</button></td>
                            </form>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col col-lg-2">
            </div>
        </div>
    </div>
</body>
</html>