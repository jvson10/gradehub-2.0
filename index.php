<html>
<head>
<title>Gradehub 2</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<form action="login_process.php" method="POST">
  <div class="container">
    <label><b>Username:</b></label>
    <input type="text" placeholder="Enter Username" name="ucode" required>

    <label><b>Password:</b></label>
    <input type="password" placeholder="Enter Password" name="pword" required>

    <button type="submit" class="btn btn-warning">Login</button>
  </div>
</form>