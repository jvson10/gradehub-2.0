<?php
    session_start();
    if(isset($_SESSION['userID'])){
        //echo "Welcome! " .$_SESSION['userID'];
    }
    include ("conn.php");

    $result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
    $row_uid = mysqli_fetch_array($result_uid);

    $result_class = mysqli_query($conn,"SELECT * FROM class_table WHERE teacher_userid = ".$_SESSION['userID']);
    $row_cid = mysqli_fetch_array($result_class);

    $result_stud = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_GET['stud_id']);
    $row_stud = mysqli_fetch_array($result_stud);
?>
<html>
<head>
<title>Update Grade  of <?php echo $_GET['activity_name']?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Welcome! Teacher <?php echo $row_uid['user_code']?></h1>
<h3>Activities for <?php echo $row_cid['class_code']?></h3>
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col col-lg-2">
            </div>
            <div class="col col-lg-auto">
                <form action="update_process.php" method="GET">
                                    <input type="number" name="class_id" value = "<?php echo $row_cid['class_id'] ?>" hidden>
                                    <input type="number" name="stud_id" value="<?php echo $row_stud['user_id']?>" hidden>
                                    <input type="number" name="stud_code" value="<?php echo $row_stud['user_code']?>" hidden>
                                    <input type="text" name="stud_fname" value="<?php echo $row_stud['user_Fname']?>" hidden>
                                    <input type="text" name="stud_lname" value="<?php echo $row_stud['user_Lname']?>" hidden>
                                    <input type="number"  name="activity_id" value="<?php echo $_GET['activity_id']?>" hidden>
                    Input new grade:<input type="number" name="new_activity_grade" value="<?php echo $_GET['activity_grade']?>" step="any">
                    <button class="btn btn-warning" type="submit">Submit</button>
                </form>
            </div>
            <div class="col col-lg-2">
            </div>
        </div>
    </div>
</body>
</html>