<?php
session_start();
if(isset($_SESSION['userID'])){
    //echo "Welcome! " .$_SESSION['userID'];
}
include ("conn.php");

$result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
$row_uid = mysqli_fetch_array($result_uid);

$result_average = mysqli_query($conn,"SELECT AVG(activity_grade) 'ave_grade' FROM activity_table WHERE student_uid = ".$_SESSION['userID']." AND activity_class_id =".$_GET['class_id']);
$res_ave = mysqli_fetch_assoc($result_average);
?>
<html>
<head>
<title><?php echo $row_uid['user_Fname']?> <?php echo $row_uid['user_Lname']?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Welcome! Student <?php echo $row_uid['user_code']?></h1>
<h3>Activities</h3>
<div class="container">
    <div class="row justify-content-lg-center">
        <div class="col col-lg-2">
        </div>
        <div class="col col-lg-auto">
            <table class="table table-hover">
                <thead>
                    <th>Activity ID</th>
                    <th>Activity Name</th>
                    <th>Grade</th>
                    <th></th>
                </thead>
                <tbody>
                <?php
                        $result = mysqli_query($conn,"SELECT * FROM activity_table WHERE activity_class_id = ".$_GET['class_id']." AND student_uid = ".$_SESSION['userID']);
                        
                        //start loop
                        while($row = mysqli_fetch_array($result)){
                ?>
                            <tr>
                                <form action="update_grade.php" method="GET">
                                    <input type="number" name="stud_id" value="<?php echo $_GET['stud_uid']?>" hidden>
                                    <td><?php echo $row['activity_id']?></td>
                                    <td><?php echo $row['activity_name']?></td>
                                    <td><?php echo $row['activity_grade']?></td>
                                </form>
                            </tr>
                        
                <?php
                        //end loop
                        }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col col-lg-2">  
            <h4>Final Grade <?php echo number_format($res_ave['ave_grade'], 1, ".", "")?></h4>
        </div>
    </div>
</div>
</body>
</html>