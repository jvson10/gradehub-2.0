<?php
    session_start();
    if(isset($_SESSION['userID'])){
        //echo "Welcome! " .$_SESSION['userID'];
    }
    include ("conn.php");
    //echo $_GET["class_code"];

    $result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
    $row_uid = mysqli_fetch_array($result_uid);

    $result_cid = mysqli_query($conn,"SELECT * FROM class_table WHERE class_code LIKE '{$_GET['class_code']}'");
    $row_cid = mysqli_fetch_array($result_cid);

?>

<html>
<head>
<title><?php echo $_GET["class_code"] ?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Welcome! Teacher <?php echo $row_uid['user_code']?></h1>
<h3>Activities for <?php echo $row_cid['class_code']?></h3>
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col col-lg-2">
            </div>
            <div class="col col-lg-auto">
                <table class="table table-hover">
                    <thead>
                        <th>Class #</th>
                        <th>Class Code</th>
                        <th>Class Name</th>
                        <th></th>
                    </thead>
                    <tbody>
                            <tr>
                                <form action="student_list.php" method="get">
                                    <td><input type="number" name="class_id" value="<?php echo $row_cid['class_id']?>" readonly></td>
                                    <td><?php echo $row_cid['class_code']?></td>
                                    <td><?php echo $row_cid['class_name']?></td>
                                    <td><button class="btn btn-warning" type="submit">Update</button></td>
                                </form>
                            </tr>
                    </tbody>
                </table>
            </div>
            <div class="col col-lg-2">
            </div>
        </div>
    </div>
</body>
</html>