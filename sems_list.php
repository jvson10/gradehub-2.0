<?php
    session_start();
    if(isset($_SESSION['userID'])){
        //echo "Welcome! " .$_SESSION['userID'];
    }
    include ("conn.php");

    $result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
    $row_uid = mysqli_fetch_array($result_uid);

    $result_cid = mysqli_query($conn,"SELECT * FROM class_table WHERE teacher_userid = ".$_SESSION['userID']);
    $row_cid = mysqli_fetch_array($result_cid);
?>
<html>
<head>
<title>List of classes by semester</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Welcome! Teacher <?php echo $row_uid['user_code']?></h1>
<h3>Activities for <?php echo $row_cid['class_code']?></h3>
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col col-lg-2">
            </div>
            <div class="col col-lg-auto">
            <table class="table table-hover">
                    <thead>
                        <th>Class #</th>
                        <th>Class Code</th>
                        <th>Class Name</th>
                        <th>Class Year</th>
                        <th>Class Semester</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php
                        $result_sems = mysqli_query($conn,"SELECT * FROM class_table WHERE teacher_userid = ".$_SESSION['userID']." ORDER BY class_table.sem ASC");
                        
                        while($row_sems = mysqli_fetch_array($result_sems)){
                        ?>
                            <tr>
                                <form action="student_list.php" method="GET">
                                    <td><input type="number" name="class_id" value="<?php echo $row_sems['class_id']?>" readonly></td>
                                    <td><?php echo $row_sems['class_code']?></td>
                                    <td><?php echo $row_sems['class_name']?></td>
                                    <td><?php echo $row_sems['year']?></td>
                                    <td><?php echo $row_sems['sem']?></td>
                                    <td><button class="btn btn-warning" type="submit">Update</button></td>
                                </form>
                            </tr>
                        
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col col-lg-2">
            </div>
        </div>
    </div>
</body>
</html>