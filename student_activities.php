<?php
    session_start();
    if(isset($_SESSION['userID'])){
        //echo "Welcome! " .$_SESSION['userID'];
    }
    include ("conn.php");

    $result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
    $row_uid = mysqli_fetch_array($result_uid);
    //echo $_SESSION['userID'];
    //echo $_GET['class_id']; 
    $result_class = mysqli_query($conn,"SELECT * FROM class_table WHERE teacher_userid = ".$_SESSION['userID']." AND class_id =".$_GET['class_id']);
    $row_cid = mysqli_fetch_array($result_class);
    

    $result_average = mysqli_query($conn,"SELECT AVG(activity_grade) 'ave_grade' FROM activity_table WHERE student_uid = ".$_GET['stud_uid']." AND activity_class_id =".$_GET['class_id']);
    $res_ave = mysqli_fetch_assoc($result_average);
?>
<html>
<head>
<title>Activities</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Welcome! Teacher <?php echo $row_uid['user_code']?></h1>
<h3>Activities for <?php echo $row_cid['class_code']?></h3>
<h3>Student: <?php echo $_GET['stud_Fname']?> <?php echo $_GET['stud_Lname']?></h3>
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col col-lg-2">
                <!-- <button class="btn btn-warning">Year</button><br />
                <button class="btn btn-warning">Semester</button><br />
                <button class="btn btn-warning">Class</button> -->
            </div>
            <div class="col col-lg-auto">
                <table class="table table-hover">
                    <thead>
                        <th>Activity ID</th>
                        <th>Activity Name</th>
                        <th>Grade</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <!--php here-->
                        <?php
                        $result = mysqli_query($conn,"SELECT * FROM activity_table WHERE activity_class_id = ".$_GET['class_id']." AND student_uid = ".$_GET['stud_uid']);
                        
                        //start loop
                        while($row = mysqli_fetch_array($result)){
                        ?>
                            <tr>
                                <form action="update_grade.php" method="GET">
                                    <input type="number" name="stud_id" value="<?php echo $_GET['stud_uid']?>" hidden>
                                    <td><input type="number" name="activity_id" value="<?php echo $row['activity_id']?>" readonly></td>
                                    <td><input type="text" name="activity_name" value="<?php echo $row['activity_name']?>" readonly></td>
                                    <td><input type="number" name="activity_grade" value="<?php echo $row['activity_grade']?>" readonly></td>
                                    <td><button class="btn btn-warning" type="submit">Update</button></td>
                                </form>
                            </tr>
                        
                        <?php
                        //end loop
                        }
                        ?>
                        <!-- end php -->
                    </tbody>
                </table>
                <form action="insert_activity.php" method="GET">
                    <input type="number" name="stud_ID" value="<?php echo $_GET['stud_uid']?>" hidden>
                    <input type="number" name="activity_class_id" value="<?php echo $row_cid['class_id']?>" hidden>
                    <input type="text" name="activity_name" value="sample name" hidden>
                    <input type="number" name="activity_grade" value="3.0" hidden>
                    <button class="btn btn-warning">Add Activity</button>
                </form>
                <h4>Final Grade <?php echo number_format($res_ave['ave_grade'], 1, ".", "")?></h4>
            </div>
            <div class="col col-lg-2">
            </div>
        </div>
    </div>
</body>
</html>