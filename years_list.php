<?php
    session_start();
    if(isset($_SESSION['userID'])){
        //echo "Welcome! " .$_SESSION['userID'];
    }
    include ("conn.php");

    $result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
    $row_uid = mysqli_fetch_array($result_uid);

    $result_cid = mysqli_query($conn,"SELECT * FROM class_table WHERE teacher_userid = ".$_SESSION['userID']);
    $row_cid = mysqli_fetch_array($result_cid);
?>
<html>
<head>
<title>List of students by year</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Welcome! Teacher <?php echo $row_uid['user_code']?></h1>
<h3>Activities for <?php echo $row_cid['class_code']?></h3>
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col col-lg-2">
            </div>
            <div class="col col-lg-auto">
                <table class="table table-hover">
                    <thead>
                        <th>User id</th>
                        <th>User code</th>
                        <th>Student first name</th>
                        <th>Student last name</th>
                        <th>Student Year</th>
                        <th>Class name</th>
                        <th>Class code</th>
                    </thead>
                    <tbody>
                        <?php

                        $result_sems = mysqli_query($conn,"SELECT * FROM users_table INNER JOIN class_table on users_table.classes_id = class_table.class_id WHERE class_table.teacher_userid = ".$_SESSION['userID']." AND users_table.user_role != 'faculty'");
                        
                        while($row_sems = mysqli_fetch_array($result_sems)){
                        
                        ?>
                            <tr>
                                <form action="update_grade.php" method="GET">
                                    <td><?php echo $row_sems['user_id']?></td>
                                    <td><?php echo $row_sems['user_code']?></td>
                                    <td><?php echo $row_sems['user_Fname']?></td>
                                    <td><?php echo $row_sems['user_Lname']?></td>
                                    <td><?php echo $row_sems['user_year']?></td>
                                    <td><?php echo $row_sems['class_name']?></td>
                                    <td><?php echo $row_sems['class_code']?></td>
                                </form>
                            </tr>
                        
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col col-lg-2">
            </div>
        </div>
    </div>
</body>
</html>