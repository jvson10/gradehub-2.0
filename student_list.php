<?php
    session_start();
    if(isset($_SESSION['userID'])){
        //echo "Welcome! " .$_SESSION['userID'];
    }
    include ("conn.php");
    $result_uid = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$_SESSION['userID']);
    $row_uid = mysqli_fetch_array($result_uid);

    $result_class = mysqli_query($conn,"SELECT * FROM class_table WHERE class_id = ".$_GET['class_id']);
    $row_cid = mysqli_fetch_array($result_class);
    //$classes_id = $_GET['classes_id'];
?>
<html>
<head>
    <title>Faculty</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <h1>Welcome! Teacher <?php echo $row_uid['user_code']?></h1>
    <h3><?php echo $row_cid['class_code']?>
        <?php echo $row_cid['class_name']?></h3>
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col col-lg-2">
                <!-- <button class="btn btn-warning">Year</button><br />
                <button class="btn btn-warning">Semester</button><br />
                <button class="btn btn-warning">Class</button> -->
            </div>
            <div class="col col-lg-auto">
                <table class="table table-hover">
                    <thead>
                        <th></th>
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </thead>
                    <tbody>
                        <?php

                        $res_studid = mysqli_query($conn,"SELECT student_userid FROM student_classes_table WHERE student_class = ".$_GET['class_id']);
                        
                        //start loop
                        while($row_studid = mysqli_fetch_array($res_studid)){
                        
                        $result = mysqli_query($conn,"SELECT * FROM users_table WHERE user_id = ".$row_studid['student_userid']);
                        $row = mysqli_fetch_array($result);
                        ?>
                            <tr>
                                <form action="student_activities.php" method="GET">
                                    <input type="number" name="class_id" value="<?php echo $_GET['class_id']?>" hidden>
                                    <td><input type="hidden" name="stud_uid" value="<?php echo $row['user_id']?>"></td>
                                    <td><input type="number" name="stud_code" value="<?php echo $row['user_code']?>"readonly></td>
                                    <td><input type="text" name="stud_Fname" value="<?php echo $row['user_Fname']?>"readonly></td>
                                    <td><input type="text" name="stud_Lname" value="<?php echo $row['user_Lname']?>"readonly></td>
                                    <td><button class="btn btn-warning" type="submit">Activities</button></td>
                                </form>
                            </tr>
                        
                        <?php
                        //end loop
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col col-lg-2"></div>
        </div>
    </div>
</body>
</html>